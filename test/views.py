from django.shortcuts import render
from django.views import View
from django.conf import settings

class ShowBookView(View):
    template_name = 'show.html'
    
    def get(self, request, *args, **kwargs):
        return render(request, self.template_name, { 'root_url': settings.ROOT_URL })
