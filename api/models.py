from django.db import models

class Books(models.Model):
        name = models.CharField(unique=True, max_length=50)
        author = models.CharField(max_length=100)

        class Meta:
           db_table = 'books'
           managed = False
           app_label = 'api'
