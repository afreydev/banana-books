from rest_framework import serializers, viewsets
from .models import Books

class BookSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Books
        fields = ('id','name','author')
