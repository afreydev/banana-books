# Banana - books
This is a little application developed using Django Framework.
You can access to the book module by "http://{{ your root_url }}/showBook".
You must edit root_url. It is located in the django settings file.
![preview](banana_books.png)
There is a db backup file in the project. It is called banana_backup.sql.
It includes the creation of a table called books.